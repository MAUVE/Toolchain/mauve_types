/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Types project.
 *
 * MAUVE Types is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Types is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_TYPES_SENSOR_GNSSTIME_HPP
#define MAUVE_TYPES_SENSOR_GNSSTIME_HPP

#include <iostream>

namespace mauve {
  namespace types {
    namespace sensor {

      /// Gnss Time (adapted from ublox PVT standard message)
      struct GnssTime
      {
        /// GPS time of week [ms]
        uint32_t itow;
        /// Year (UTC)
        uint16_t year;
        /// Month, range 1..12 (UTC)
        uint8_t month;
        /// Day of month, range 1..31 (UTC)
        uint8_t day;
        /// Hour of day, range 0..23 (UTC)
        uint8_t hour;
        /// Minute of hour, range 0..59 (UTC)
        uint8_t min;
        /// Seconds of minute, range 0..60 (UTC)
        uint8_t sec;
        /// Validity flag
        uint8_t valid;
        /// Time accuracy estimate [ns] (UTC)
        uint32_t t_acc;
        /// Fraction of a second [ns], range -1e9 .. 1e9 (UTC)
        int32_t nano;
      };

      namespace GnssTimeFlags
      {
        const uint8_t VALID_DATE = 0x01;
        const uint8_t VALID_TIME = 0x02;
        const uint8_t FULLY_RESOLVED = 0x04;
      };

      /// Stream a Gnss Time
      std::ostream& operator<<(std::ostream&, const GnssTime &);

    }
  }
}

#endif
