/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Types project.
 *
 * MAUVE Types is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Types is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_TYPES_SENSOR_GNSSSATINFO_HPP
#define MAUVE_TYPES_SENSOR_GNSSSATINFO_HPP

#include <iostream>

namespace mauve {
  namespace types {
    namespace sensor {

      struct SatInfo
      {
        uint8_t gnss_id;   /// GNSS identifier
        uint8_t sv_id;     /// Satellite identifier
        uint8_t cno;       /// Carrier to noise ration dBHz
        int8_t elev;       /// Satellite elevation [deg]
        int16_t azim;      /// Satellite azimut [deg]
        int16_t pr_res;    /// Pseudo range residual [0.1m]
        uint32_t flags;    ///
      };

      namespace SV_FLAGS
      {
        const static uint32_t SV_USED = 0x08;
      };

      struct GnssSatInfo
      {
        uint32_t itow;
        uint8_t version;
        uint8_t numSvs;
        uint8_t reserved[2];

        std::vector<SatInfo> sv;
      };


    }
  }
}

#endif
