/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Types project.
 *
 * MAUVE Types is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Types is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_TYPES_SENSOR_LASERSCAN_HPP
#define MAUVE_TYPES_SENSOR_LASERSCAN_HPP

#include <iostream>
#include <vector>

namespace mauve {
namespace types {
namespace sensor {

  /** Structure for a single scan from a planar laser range finder.
   * inspired from ROS sensor_msgs/LaserScan.
   */
  struct LaserScan {
    /** start angle of the scan (rad). */
    double angle_min;
    /** end angle of the scan (rad). */
    double angle_max;
    /** increment between two successive angles (rad). */
    double angle_increment;
    /** minimum range value (m). */
    double range_min;
    /** maximum range value (m). */
    double range_max;
    /** range data (m). values < range_min or > range_max should be discarded. */
    std::vector<double> ranges;
  };

  /** Stream a LaserScan */
  std::ostream& operator<< (std::ostream& out, const LaserScan& scan);

}}}

#endif // MAUVE_TYPES_SENSOR_LASERSCAN_HPP
