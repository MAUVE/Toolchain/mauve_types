/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Types project.
 *
 * MAUVE Types is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Types is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_TYPES_SENSOR_GNSSSTATUS_HPP
#define MAUVE_TYPES_SENSOR_GNSSSTATUS_HPP

#include <iostream>

namespace mauve {
  namespace types {
    namespace sensor {

      /** GNSS Service */
      enum struct GNSS_SERVICE {
        GPS = 1,
        GLONASS = 2,
        COMPASS = 4,
        GALILEO = 8
      };

      /** GNSS Fix Status */
      enum struct FIX_STATUS {
        NO_FIX = -1,
        FIX = 0,
        SBAS_FIX = 1,
        GBAS_FIX = 2
      };

      /** GNSS Status type */
      struct GNSSStatus {
        /** status */
        FIX_STATUS status;
        /** service */
        GNSS_SERVICE service;
      };

      /** Stream a GNSS Service */
      std::ostream& operator<<(std::ostream&, const GNSS_SERVICE&);
      /** Stream a Fix Status */
      std::ostream& operator<<(std::ostream&, const FIX_STATUS&);
      /** Stream a GNSS Status */
      std::ostream& operator<<(std::ostream&, const GNSSStatus&);

    }
  }
}

#endif // MAUVE_TYPES_SENSOR_GNSSSTATUS_HPP
