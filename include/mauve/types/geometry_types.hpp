/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Types project.
 *
 * MAUVE Types is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Types is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
 #ifndef MAUVE_TYPES_GEOMETRY_HPP
#define MAUVE_TYPES_GEOMETRY_HPP

#include "geometry/Vector3.hpp"
#include "geometry/Quaternion.hpp"
#include "geometry/Point2D.hpp"
#include "geometry/Point.hpp"
#include "geometry/Pose2D.hpp"
#include "geometry/Pose.hpp"
#include "geometry/PoseStamped.hpp"
#include "geometry/Transform.hpp"
#include "geometry/TransformStamped.hpp"
#include "geometry/UnicycleVelocity.hpp"
#include "geometry/WheelVelocity.hpp"
#include "geometry/MapMetaData.hpp"
#include "geometry/OccupancyGrid.hpp"

#endif // MAUVE_TYPES_GEOMETRY_HPP
