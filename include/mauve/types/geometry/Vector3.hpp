/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Types project.
 *
 * MAUVE Types is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Types is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_TYPES_GEOMETRY_VECTOR3_HPP
#define MAUVE_TYPES_GEOMETRY_VECTOR3_HPP

#include <iostream>

namespace mauve {
  namespace types {
    namespace geometry {

      /**
       * Structure of a Vector in 3D space
       * It is defined by \a x, \a y, \a z components.
       */
      struct Vector3 {
        /** X component */
        double x;
        /** Y component */
        double y;
        /** Z component */
        double z;
      };

      /** Stream a Vector3 */
      std::ostream& operator<< (std::ostream& out, const Vector3& p);

    } // namespace geometry
  } // namespace types
} // namespace mauve

#endif // MAUVE_TYPES_GEOMETRY_VECTOR3_HPP
