/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Types project.
 *
 * MAUVE Types is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Types is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_TYPES_GEOMETRY_UNICYCLEVELOCITY_HPP
#define MAUVE_TYPES_GEOMETRY_UNICYCLEVELOCITY_HPP

#include <iostream>

namespace mauve {
  namespace types {
    namespace geometry {

      /** Structure of velocity for a unicycle robot.
       */
      struct UnicycleVelocity {
        /** linear velocity (towards X axis) */
        double linear;
        /** angular velocity (around Z axis) */
        double angular;
      };

      /** Stream a UnicycleVelocity.
       */
      std::ostream& operator<< (std::ostream&, const UnicycleVelocity&);

    }
  }
}

#endif // MAUVE_TYPES_GEOMETRY_UNICYCLEVELOCITY_HPP
