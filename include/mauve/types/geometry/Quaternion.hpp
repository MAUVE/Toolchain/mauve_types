/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Types project.
 *
 * MAUVE Types is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Types is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_TYPES_GEOMETRY_QUATERNION_HPP
#define MAUVE_TYPES_GEOMETRY_QUATERNION_HPP

#include <iostream>

namespace mauve {
  namespace types {
    namespace geometry {

      /**
       * Structure of a Quaternion
       * It is defined by \a x, \a y, \a z and \a w components.
       */
      struct Quaternion {
        /** Scalar component */
        double w;
        /** X component */
        double x;
        /** Y component */
        double y;
        /** Z component */
        double z;
      };

      /** Stream a Quaternion */
      std::ostream& operator<< (std::ostream& out, const Quaternion& q);

    } // namespace geometry
  } // namespace types
} // namespace mauve

#endif // MAUVE_TYPES_GEOMETRY_QUATERNION_HPP
