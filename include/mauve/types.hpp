/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Types project.
 *
 * MAUVE Types is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Types is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_TYPES_HPP
#define MAUVE_TYPES_HPP

#include "types/geometry_types.hpp"
#include "types/sensor_types.hpp"

namespace mauve {

  /** Contains common types for robotic applications. */
  namespace types {

    /** Contains common types for geometric computations. */
    namespace geometry {}

    /** Contains common types for sensor measurements. */
    namespace sensor {}

  }
}

#endif // MAUVE_TYPES_HPP
