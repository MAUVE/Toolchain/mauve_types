# MAUVE Types

This package provides basic types for MAUVE architectures that are
commonly used in robotics.

Most of the are inspired from ROS messages.

MAUVE Types is licensed under the
[GNU Lesser General Public License](https://www.gnu.org/licenses/lgpl-3.0).

## Instal Instructions

General instructions are given in the [Toolchain Readme](https://gitlab.com/mauve/mauve_toolchain).

To install only the MAUVE Types package:
```
mkdir -p ~/mauve_ws/src
cd ~/mauve_ws
git clone https://gitlab.com/MAUVE/mauve_types.git src/mauve_types
catkin_make
```

## Documentation

The reference manual, the API documentation and some tutorials are available on https://mauve.gitlab.io
