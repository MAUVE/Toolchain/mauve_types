#include "mauve/types/geometry/Quaternion.hpp"
#include <iomanip>

namespace mauve {
namespace types {
namespace geometry {

  std::ostream& operator<< (std::ostream& out, const Quaternion& q)
  {
    out << std::fixed<<std::setprecision(6);
    out << "(" << q.w << "; " << q.x << ", " << q.y << ", " << q.z << ")";
    out << std::resetiosflags(std::ios_base::basefield);
    return out;
  }

}
}
}
