#include "mauve/types/sensor/ImuState.hpp"

namespace mauve {
namespace types {
namespace sensor
{

  std::ostream& operator<< (std::ostream& out, const ImuState& imu)
  {
    out << "Timestamp    = " << imu.timestamp/1000 << " us" <<std::endl;
    out << "Orientation  = " << imu.orientation << std::endl;
    out << "Angular vel  = " << imu.angular_velocity << std::endl;
    out << "Acceleration = " << imu.linear_acceleration << std::endl;
    out << "Magnetometer = " << imu.magnetic_field << std::endl;
    out << "Ekf init     = " << imu.ekf_init_valid << std::endl;
    out << "Ekf heading  = " << imu.ekf_heading_valid << std::endl;
    out << "Gyro valid   = " << imu.angular_velocity_valid << std::endl;
    out << "Accel valid  = " << imu.linear_acceleration_valid << std::endl;
    out << "Magnet valid = " << imu.magnetic_field_valid << std::endl;

    return out;
  }

}
}
}
