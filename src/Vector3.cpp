#include "mauve/types/geometry/Vector3.hpp"
#include <iomanip>

namespace mauve {
namespace types {
namespace geometry {

  std::ostream& operator<< (std::ostream& out, const Vector3& v)
  {
    out << std::fixed<<std::setprecision(6);
    out << "(" << v.x << ", " << v.y << ", " << v.z << ")";
    out << std::resetiosflags(std::ios_base::basefield);
    return out;
  }

}
}
}
