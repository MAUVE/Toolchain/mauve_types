/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Types project.
 *
 * MAUVE Types is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Types is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#include "mauve/types/sensor/Image.hpp"

namespace mauve {
  namespace types {
    namespace sensor {

Image::Image()
  : encoding("")
  , width(0)
  , height(0)
  , image { nullptr }
{}

Image& Image::operator=(const Image& other) {
  width = other.width;
  height = other.height;
  encoding = other.encoding;
  image = other.image; //.clone();    //FIXME: this is not a copy! data are shared! did you mean other.image.copyTo(image)
  return *this;
}

std::ostream& operator<< (std::ostream& out, const Image& image) {
  out << "image (" << image.width << "x"
    << image.height << ";" << image.encoding << ")";
  return out;
}

    }
  }
}
